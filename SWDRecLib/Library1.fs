﻿namespace SWDRecLib
type Account(number : int, holder : string) = class
    let mutable amount = 0m
 
    member x.Number = number
    member x.Holder = holder
    member x.Amount = amount
    member x.X = x

    member x.Deposit(value) = amount <- amount + value
    member x.Withdraw(value) = amount <- amount - value
end
