﻿namespace MyCompany.MyLibrary 
module Module1 = 
    let x = 1
    let rec fact x =
        if x < 1 then 1
        else x * fact (x - 1)
