﻿using System;
using GalaSoft.MvvmLight.Messaging;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SWDLibDemo;
using SWDLibDemo.Model;
using SWDProjektGUI.Messages;
using SWDProjektGUI.Model;
using SWDProjektGUI.ViewModel;

namespace SWDTest
{
	[TestClass]
	public class UnitTest1
	{
		private ViewModelLocator xxx = new ViewModelLocator();

		[TestMethod]
		public void TestWczytywania()
		{
			//wczytanie pliku
			Messenger.Default.Register<LoadFileMessage>(this, rfm => rfm.Execute("testLoad2.xml"));
			var mainVM = xxx.Main;
			mainVM.LoadFileCommand.Execute(null);

			Assert.AreEqual(StatusDnia.Nigdy, mainVM.DanePlanu.GetStatusDnia(DzienTygodnia.Wtorek, GodzinaZajec.Od0915));

			Assert.AreEqual("LabA", mainVM.DanePlanu.GrupyZajec[0].NazwaGrupy);
			Assert.AreEqual("L00", mainVM.DanePlanu.GrupyZajec[0].Terminy[0].KodKursu);
			Assert.AreEqual(GodzinaZajec.Od0730, mainVM.DanePlanu.GrupyZajec[0].Terminy[0].GodzinaZajec);

			Assert.AreEqual("L01", mainVM.DanePlanu.GrupyZajec[0].Terminy[1].KodKursu);
			Assert.AreEqual(GodzinaZajec.Od0915, mainVM.DanePlanu.GrupyZajec[0].Terminy[1].GodzinaZajec);

			Assert.AreEqual("L02", mainVM.DanePlanu.GrupyZajec[0].Terminy[2].KodKursu);
			Assert.AreEqual(Ocena.OcenaPlus2, mainVM.DanePlanu.GrupyZajec[0].Terminy[2].OcenaZajec);
			Assert.AreEqual(GodzinaZajec.Od1115, mainVM.DanePlanu.GrupyZajec[0].Terminy[2].GodzinaZajec);

			Assert.AreEqual("LabB", mainVM.DanePlanu.GrupyZajec[1].NazwaGrupy);
			Assert.AreEqual("L03", mainVM.DanePlanu.GrupyZajec[1].Terminy[0].KodKursu);
			Assert.AreEqual(GodzinaZajec.Od0730, mainVM.DanePlanu.GrupyZajec[1].Terminy[0].GodzinaZajec);
			Assert.AreEqual("L04", mainVM.DanePlanu.GrupyZajec[1].Terminy[1].KodKursu);
			Assert.AreEqual(GodzinaZajec.Od0915, mainVM.DanePlanu.GrupyZajec[1].Terminy[1].GodzinaZajec);
			Assert.AreEqual(Ocena.OcenaZero, mainVM.DanePlanu.GrupyZajec[1].Terminy[1].OcenaZajec);
			Assert.AreEqual("L05", mainVM.DanePlanu.GrupyZajec[1].Terminy[2].KodKursu);
			Assert.AreEqual(GodzinaZajec.Od1115, mainVM.DanePlanu.GrupyZajec[1].Terminy[2].GodzinaZajec);

			Assert.AreEqual("WykA", mainVM.DanePlanu.GrupyZajec[2].NazwaGrupy);
			Assert.AreEqual("W00", mainVM.DanePlanu.GrupyZajec[2].Terminy[0].KodKursu);
			Assert.AreEqual(GodzinaZajec.Od0730, mainVM.DanePlanu.GrupyZajec[2].Terminy[0].GodzinaZajec);
			Assert.AreEqual(Ocena.OcenaPlus3, mainVM.DanePlanu.GrupyZajec[2].Terminy[0].OcenaZajec);
			Assert.AreEqual("W01", mainVM.DanePlanu.GrupyZajec[2].Terminy[1].KodKursu);
			Assert.AreEqual(GodzinaZajec.Od0915, mainVM.DanePlanu.GrupyZajec[2].Terminy[1].GodzinaZajec);

			Assert.AreEqual("WykB", mainVM.DanePlanu.GrupyZajec[3].NazwaGrupy);
			Assert.AreEqual("W02", mainVM.DanePlanu.GrupyZajec[3].Terminy[0].KodKursu);
			Assert.AreEqual(GodzinaZajec.Od0730, mainVM.DanePlanu.GrupyZajec[3].Terminy[0].GodzinaZajec);
			Assert.AreEqual("W03", mainVM.DanePlanu.GrupyZajec[3].Terminy[1].KodKursu);
			Assert.AreEqual(GodzinaZajec.Od0915, mainVM.DanePlanu.GrupyZajec[3].Terminy[1].GodzinaZajec);
			for (var g = 0; g < 3; g++)
			{
				var grupa = mainVM.DanePlanu.GrupyZajec[g];
				var dzien = (g == 2) ? DzienTygodnia.Wtorek : DzienTygodnia.Poniedzialek;
				foreach (var termin in grupa.Terminy)
				{
					Assert.AreEqual(dzien, termin.DzienTygodnia);
				}
			}

		}

		[TestMethod]
		public void TestZapisywania()
		{
			//zapisanie pliku
			Messenger.Default.Register<LoadFileMessage>(this, rfm => rfm.Execute("testLoad.xml"));
			var mainVM = xxx.Main;
			mainVM.LoadFileCommand.Execute(null);

			Messenger.Default.Register<SaveFileMessage>(this, rfm => rfm.Execute("testSave.xml"));

			mainVM.SaveFileCommand.Execute(null);

			var contentA = System.IO.File.ReadAllText("testLoad.xml");
			var contentB = System.IO.File.ReadAllText("testSave.xml");
			Assert.AreEqual(contentA, contentB);

		}

		[TestMethod]
		public void TestUkladaniaPlanu()
		{
			//wczytanie pliku
			Messenger.Default.Register<LoadFileMessage>(this, rfm => rfm.Execute("testLoad.xml"));
			var mainVM = xxx.Main;
			mainVM.LoadFileCommand.Execute(null);

			//wyznaczenie planu
			xxx.Main.MakePlanCommand.Execute(null);

			var optPlan = xxx.Main.DanePlanu.OptymalnePlany;
			Assert.AreEqual(1, optPlan.Count);
			Assert.AreEqual(4, optPlan[0].Zajecia.Count);
			Assert.AreEqual("W03", optPlan[0].Zajecia[0].KodKursu);
			Assert.AreEqual("W00", optPlan[0].Zajecia[1].KodKursu);
			Assert.AreEqual("L03", optPlan[0].Zajecia[2].KodKursu);
			Assert.AreEqual("L02", optPlan[0].Zajecia[3].KodKursu);
		}

		[TestMethod]
		public void TestZmianyGrupy()
		{
			Messenger.Default.Register<LoadFileMessage>(this, rfm => rfm.Execute("testLoad.xml"));
			var mainVM = xxx.Main;
			mainVM.LoadFileCommand.Execute(null);


			var termin = xxx.Main.DanePlanu.GrupyZajec[0].Terminy[0];
			termin.Grupa = xxx.Main.DanePlanu.GrupyZajec[1];
			Assert.IsTrue(xxx.Main.DanePlanu.GrupyZajec[1].Terminy.Contains(termin));
		}

		[TestMethod]
		public void TestDodaniaGrupy()
		{
			Messenger.Default.Register<CreateNewGrupaMessage>(this, cngm => Messenger.Default.Send(new NewGrupaCreatedMessage()
				                                                                                       {
					                                                                                       NazwaGrupy = "NOWA GRUPA"
				                                                                                       }
				                                                                ));
			var mainVM = xxx.Main;
			mainVM.AddGrupaCommand.Execute(null);

			Assert.AreEqual("NOWA GRUPA", mainVM.DanePlanu.GrupyZajec[0].NazwaGrupy);
		}

		[TestMethod]
		public void TestDodaniaTerminu()
		{
			var mainVM = xxx.Main;

			Messenger.Default.Register<CreateNewGrupaMessage>(this, cngm => Messenger.Default.Send(new NewGrupaCreatedMessage()
				                                                                                       {
					                                                                                       NazwaGrupy = "NOWA GRUPA"
				                                                                                       }
				                                                                ));

			mainVM.AddGrupaCommand.Execute(null);


			int l = mainVM.DanePlanu.GrupyZajec[0].Terminy.Count;
			mainVM.CurrentGrupa = mainVM.DanePlanu.GrupyZajec[0];
			mainVM.AddTerminCommand.Execute(null);

			Assert.AreEqual(l + 1, mainVM.DanePlanu.GrupyZajec[0].Terminy.Count);

		}

		[TestMethod]
		public void TestUsunieciaTerminu()
		{

			Messenger.Default.Register<LoadFileMessage>(this, rfm => rfm.Execute("testLoad.xml"));
			Messenger.Default.Register<DialogMessage>(this, rfm => rfm.ProcessCallback(System.Windows.MessageBoxResult.Yes));
			var mainVM = xxx.Main;
			mainVM.LoadFileCommand.Execute(null);

			var termToRemove = mainVM.DanePlanu.GrupyZajec[0].Terminy[0];
			mainVM.CurrentTermin = termToRemove;
			mainVM.RemoveSelectedCommand.Execute(null);


			var contains = mainVM.DanePlanu.GrupyZajec[0].Terminy.Contains(termToRemove);
			Assert.IsFalse(contains);
		}

		[TestMethod]
		public void TestUsunieciaGrupy()
		{
	
			Messenger.Default.Register<LoadFileMessage>(this, rfm => rfm.Execute("testLoad.xml"));
			Messenger.Default.Register<DialogMessage>(this, rfm => rfm.ProcessCallback(System.Windows.MessageBoxResult.Yes));
			var mainVM = xxx.Main;
			mainVM.LoadFileCommand.Execute(null);

			var termToRemove = mainVM.DanePlanu.GrupyZajec[0];
			mainVM.CurrentGrupa = termToRemove;
			mainVM.RemoveSelectedCommand.Execute(null);
			var contains = mainVM.DanePlanu.GrupyZajec.Contains(termToRemove);
			Assert.IsFalse(contains);
		}

		[TestMethod]
		public void TestWyboruPlanu()
		{
	
			Messenger.Default.Register<LoadFileMessage>(this, rfm => rfm.Execute("testLoad2.xml"));
			var mainVM = xxx.Main;
			mainVM.LoadFileCommand.Execute(null);
			var plan = mainVM.OptymalnePlany[0];
			Assert.AreSame(mainVM.DanePlanu.OptymalnePlany[0], plan.OptymalnyPlan);
			mainVM.CurrentPlan = plan;
			Assert.IsFalse(mainVM.WygladPlanu[0].IsEmpty);
			Assert.IsFalse(mainVM.WygladPlanu[1].IsEmpty);
			Assert.IsFalse(mainVM.WygladPlanu[2].IsEmpty);
			Assert.IsFalse(mainVM.WygladPlanu[7].IsEmpty);

			Assert.AreEqual("LabB\r\n\r\nL03", mainVM.WygladPlanu[0].Text);
			Assert.AreEqual("WykB\r\n\r\nW03", mainVM.WygladPlanu[1].Text);
			Assert.AreEqual("LabA\r\n\r\nL02", mainVM.WygladPlanu[2].Text);
			Assert.AreEqual("WykA\r\n\r\nW00", mainVM.WygladPlanu[7].Text);

			for (int i = 0; i < 49; i++)
			{
				if (i == 0 || i == 1 || i == 2 || i == 7) continue;
				Assert.IsTrue(mainVM.WygladPlanu[i].IsEmpty);
				Assert.IsTrue(string.IsNullOrWhiteSpace(mainVM.WygladPlanu[i].Text));
			}

		}

		[TestMethod]
		public void TestZmianyOcenyTerminu()
		{
			//zmiana oceny
			var mainVM = xxx.Main;

			Messenger.Default.Register<CreateNewGrupaMessage>(this, cngm => Messenger.Default.Send(new NewGrupaCreatedMessage()
				                                                                                       {
					                                                                                       NazwaGrupy = "NOWA GRUPA"
				                                                                                       }
				                                                                ));

			mainVM.AddGrupaCommand.Execute(null);


			int l = mainVM.DanePlanu.GrupyZajec[0].Terminy.Count;
			mainVM.CurrentGrupa = mainVM.DanePlanu.GrupyZajec[0];
			mainVM.AddTerminCommand.Execute(null);


			var term = mainVM.DanePlanu.GrupyZajec[0].Terminy[0];
			mainVM.CurrentTermin = term;
			mainVM.CurrentOcena = 2;



			Assert.AreEqual(Ocena.OcenaPlus3, term.OcenaZajec);

		}

		[TestMethod]
		public void TestZmianyOcenyDnia()
		{
			//Zmiana oceny dnia
			var mainVM = xxx.Main;
			Assert.AreEqual(StatusDnia.Neutralny, mainVM.DniGodziny[11].State);
			mainVM.DniGodziny[11].FlipFlop.Execute(null);
			Assert.AreEqual(StatusDnia.JakTrzeba, mainVM.DniGodziny[11].State);
			mainVM.DniGodziny[11].FlipFlop.Execute(null);
			Assert.AreEqual(StatusDnia.Nigdy, mainVM.DniGodziny[11].State);
			mainVM.DniGodziny[11].FlipFlop.Execute(null);
			Assert.AreEqual(StatusDnia.Neutralny, mainVM.DniGodziny[11].State);
		}

		[TestCleanup]
		public void CleanMess()
		{
			xxx.Main.NewCommand.Execute(null);
			Messenger.Default.Unregister(this);
		}
	}
}
