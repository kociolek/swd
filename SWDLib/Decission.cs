﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SWDLib
{
    public abstract class Decission
    {
        public override abstract int GetHashCode();
        public override abstract bool Equals(object obj);
    }

}
