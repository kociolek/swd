﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SWDLib
{
	public abstract class State<TStan, TDecyzja>
		where TDecyzja : Decission
			where TStan : State<TStan,TDecyzja>
	{
		public abstract TStan AdvanceToNext(TDecyzja u);
			
		public override abstract int GetHashCode();
		public override abstract bool Equals(object obj);
	}
	public abstract class Decission
	{
		public override abstract int GetHashCode();
		public override abstract bool Equals(object obj);
	}

	public abstract class DpSolver<TStan, TDecyzja> where TStan:State<TStan,TDecyzja>
													where TDecyzja:Decission
	{
		private int mN;

		public int N
		{
			get { return mN; }
			set
			{
				if (mN != value)
				{
					int old = mN;
					mN = value;
					NChanged(old);
				}
			}
		}

		private readonly Dictionary<TStan, Dictionary<TDecyzja, float>> mMemory;

		protected abstract TStan GenerateEmptyState();
		protected virtual void NChanged(int oldN)
		{
		}

		public abstract float Rank(TStan xn, TDecyzja un);
		protected abstract List<TDecyzja> GetDicissions(int n, TStan xn);

		protected DpSolver()
        {
            mMemory = new Dictionary<TStan, Dictionary<TDecyzja, float>>();
        }
        protected virtual void ClearImpl()
        {
        }

	    public void Clear()
        {
            mMemory.Clear();
            ClearImpl();
        }
		
		private float DPStep(TStan stan, int n)
		{
			//todo: remove recursion
			var dostepneDecyzje = GetDicissions(n, stan);

			if (dostepneDecyzje.Count == 0)
				return float.NegativeInfinity;

			var bestValue = float.NegativeInfinity;
			for (int d = 0; d < dostepneDecyzje.Count; d++)
			{
				var decission = dostepneDecyzje[d];

				//sprawdzenie czy juz wyliczone
				Dictionary<TDecyzja, float> memorizedResults;
				if(!mMemory.TryGetValue(stan, out memorizedResults))
					memorizedResults = mMemory[stan] = new Dictionary<TDecyzja, float>();
				
				float x;
				if (!memorizedResults.TryGetValue(decission, out x))
				{
					var nextState = stan.AdvanceToNext(decission);

					x = Rank(stan, decission);

					if (n + 1 != N)
						x += DPStep(nextState, n + 1);
					memorizedResults[decission] = x;
				}

				if (x > bestValue)
					bestValue = x;
				
			}
			return bestValue;
		}

		public class SolutionNode
		{
			public TDecyzja Decyzja;
			public SolutionNode[] SubNodes;
		}
		private SolutionNode[] Podstawianie(TStan stan0, int n=0)
		{
			if (n < N)
			{
				var steps0 = mMemory[stan0];
				var bestStepValue = steps0.Max(step => step.Value);
				var bestSteps = steps0.Where(step => step.Value == bestStepValue).Select(step => step.Key).ToArray();
				var solutions = new SolutionNode[bestSteps.Length];
				for (int i = 0; i < solutions.Length; i++)
				{
					solutions[i] = new SolutionNode()
						               {Decyzja = bestSteps[i], SubNodes = Podstawianie(stan0.AdvanceToNext(bestSteps[i]), n + 1)};
				}
				return solutions;
			}
			return null;
		}
		public SolutionNode[] Solve()
		{
			var stan = GenerateEmptyState();

			var isSolvable = DPStep(stan, 0);
			if (float.IsInfinity(isSolvable))
				return null;
			//get optimal path
			var paths = Podstawianie(stan);
			return paths;
		}
    }
}
