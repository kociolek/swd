﻿using System;
using SWDLibDemo.Model;

namespace SWDLibDemo
{
	internal class Program
	{
		private static void Main(string[] args)
		{
			var dp = new SWDProjektGUI.Model.DanePlanu();
			dp.Read("plan.xml");
			ISolver solverInterface = new SolverWrapper();
			var plan = solverInterface.Solve(dp);
			dp.SetPlan(plan);
			dp.Write("plan2.xml");

			
			Console.WriteLine();
			Console.WriteLine("Optymalny plan(y):");
			for (int i = 0; i < dp.OptymalnePlany.Count; i++)
			{
				var op = dp.OptymalnePlany[i];
				for (int z = 0; z < op.Zajecia.Count;z++ )
				{
					var zajecia = op.Zajecia[z];
					Console.WriteLine("{0}|{1}\t{2} - {3}", zajecia.KodKursu, zajecia.DzienTygodnia, zajecia.GodzinaZajec,zajecia.Opis);
				}
			}
			Console.ReadLine();
		}
	}
}
