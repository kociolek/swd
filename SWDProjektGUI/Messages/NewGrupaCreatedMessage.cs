﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GalaSoft.MvvmLight.Messaging;

namespace SWDProjektGUI.Messages
{
	public class NewGrupaCreatedMessage:MessageBase
	{
		public string NazwaGrupy { get; set; }
	}
}
