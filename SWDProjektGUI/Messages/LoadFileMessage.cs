﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SWDProjektGUI.Messages
{
	public class LoadFileMessage : GalaSoft.MvvmLight.Messaging.NotificationMessageAction<string>
	{
		public LoadFileMessage(string notification, Action<string> callback) : base(notification, callback)
		{
		}

		public LoadFileMessage(object sender, string notification, Action<string> callback) : base(sender, notification, callback)
		{
		}

		public LoadFileMessage(object sender, object target, string notification, Action<string> callback) : base(sender, target, notification, callback)
		{
		}
	}
}
