﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SWDProjektGUI.Messages
{
	public class SaveFileMessage : GalaSoft.MvvmLight.Messaging.NotificationMessageAction<string>
	{
		public SaveFileMessage(string notification, Action<string> callback) : base(notification, callback)
		{
		}

		public SaveFileMessage(object sender, string notification, Action<string> callback) : base(sender, notification, callback)
		{
		}

		public SaveFileMessage(object sender, object target, string notification, Action<string> callback) : base(sender, target, notification, callback)
		{
		}
	}
}
