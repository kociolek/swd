﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GalaSoft.MvvmLight.Messaging;
using SWDLibDemo.Model;
using SWDProjektGUI.Model;

namespace SWDProjektGUI.Messages
{
	public class CreateNewGrupaMessage:MessageBase
	{
		public DanePlanu GrupaZajec { get; set; }
	}
}
