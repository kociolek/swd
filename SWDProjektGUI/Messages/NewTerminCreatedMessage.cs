﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GalaSoft.MvvmLight.Messaging;

namespace SWDProjektGUI.Messages
{
	public class NewTerminCreatedMessage:MessageBase
	{
		public string KodKursu { get; set; }
	}
}
