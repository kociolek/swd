﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GalaSoft.MvvmLight;

namespace SWDProjektGUI.ViewModel
{
	public class PlanEntryViewModel:ViewModelBase
	{
		/// <summary>
		/// The <see cref="Text" /> property's name.
		/// </summary>
		public const string TextPropertyName = "Text";

		private string mText = string.Empty;

		/// <summary>
		/// Sets and gets the Text property.
		/// Changes to that property's value raise the PropertyChanged event. 
		/// </summary>
		public string Text
		{
			get
			{
				return mText;
			}
			set
			{
				Set(() => Text, ref mText, value);
			}
		}
		/// <summary>
		/// The <see cref="IsEmpty" /> property's name.
		/// </summary>
		public const string IsEmptyPropertyName = "IsEmpty";

		private bool mIsEmpty = true;

		/// <summary>
		/// Sets and gets the IsEmpty property.
		/// Changes to that property's value raise the PropertyChanged event. 
		/// </summary>
		public bool IsEmpty
		{
			get
			{
				return mIsEmpty;
			}
			set
			{
				Set(() => IsEmpty, ref mIsEmpty, value);
			}
		}
	}
}
