﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using System.Text;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using SWDLibDemo;
using SWDProjektGUI.Model;

namespace SWDProjektGUI.ViewModel
{
    public class DayTimeViewModel : ViewModelBase
    {
	    private MainViewModel mMvm;
	    private DzienTygodnia mDzien;
	    private GodzinaZajec mGodzina;

		private static string[] dniTygodnia = new[] { "pon.", "wt.", "śr", "czw", "pt", "so", "nd" };
		private static string[] godziny = new[] { "7:30 - 9:00", "9:15 - 11:00", "11:15 - 13:00", "13:15 - 15:00", "15:15 - 17:00", "17:05 - 18:45", "18:55 - 20:35" };

		public DayTimeViewModel(MainViewModel mvm, DzienTygodnia dzien, GodzinaZajec godzina)
		{
			mDzien = dzien;
			mGodzina = godzina;
			mMvm = mvm;
			mvm.PropertyChanged += MvmOnPropertyChanged;
			Text = string.Format("{0} {1}", dniTygodnia[(int) dzien], godziny[(int) godzina]);
		}

	    private void MvmOnPropertyChanged(object sender, PropertyChangedEventArgs propertyChangedEventArgs)
	    {
		    if(propertyChangedEventArgs.PropertyName == "DanePlanu")
		    {
			    if(mMvm.DanePlanu!=null)
			    {
				    State = mMvm.DanePlanu.GetStatusDnia(mDzien, mGodzina);
					mMvm.DanePlanu.ZajeteTerminy.CollectionChanged += ZajeteTerminyOnCollectionChanged;
			    }
		    }
	    }

	    private void ZajeteTerminyOnCollectionChanged(object sender, NotifyCollectionChangedEventArgs notifyCollectionChangedEventArgs)
	    {
			
			State = mMvm.DanePlanu.GetStatusDnia(mDzien,mGodzina);// .ZajeteTerminy.Any(x => x.Dzien == mDzien && x.GodzinaDnia == mGodzina);

	    }

	 

	    /// <summary>
        /// The <see cref="Text" /> property's name.
        /// </summary>
        public const string TextPropertyName = "Text";

        private string mText = default(string);

        /// <summary>
        /// Sets and gets the Text property.
        /// Changes to that property's value raise the PropertyChanged event. 
        /// </summary>
        public string Text
        {
            get
            {
                return mText;
            }
            set
            {
                Set(() => Text, ref mText, value);
            }
        }
        /// <summary>
        /// The <see cref="State" /> property's name.
        /// </summary>
        public const string StatePropertyName = "State";

        private StatusDnia mState = StatusDnia.Neutralny;

        /// <summary>
        /// Sets and gets the State property.
        /// Changes to that property's value raise the PropertyChanged event. 
        /// </summary>
        public StatusDnia State
        {
            get
            {
                return mState;
            }
            set
            {
				if (Set(() => State, ref mState, value))
				{
					if (mMvm.DanePlanu != null)
						mMvm.DanePlanu.SetZajetyTermin(mDzien, mGodzina, mState);
					/*if (mState)
					{
						if(mMvm.DanePlanu!=null)
						mMvm.DanePlanu.DodajZajetyTermin(mDzien, mGodzina);
					}
					else
					{
						if (mMvm.DanePlanu != null)
						mMvm.DanePlanu.UsunZajetyTermin(mDzien, mGodzina);
					}*/
				}
            }
        }
        private RelayCommand mFlipFlop;

        /// <summary>
        /// Gets the FlipFlop.
        /// </summary>
        public RelayCommand FlipFlop
        {
            get
            {
                return mFlipFlop
                    ?? (mFlipFlop = new RelayCommand(ExecuteFlipFlop));
            }
        }

        private void ExecuteFlipFlop()
        {
	        State = (StatusDnia) (((int) State + 1)%3);
        }
    }
}
