﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using SWDLibDemo.Model;
using SWDProjektGUI.Messages;
using SWDProjektGUI.Model;

namespace SWDProjektGUI.ViewModel
{
	class NewGrupaViewModel:ViewModelBase
	{
		public DanePlanu GrupaZajec { get; set; }

		private RelayCommand mOkCommand;

		/// <summary>
		/// Gets the OkCommand.
		/// </summary>
		public RelayCommand OkCommand
		{
			get
			{
				return mOkCommand ?? (mOkCommand = new RelayCommand(
					ExecuteOkCommand,
					CanExecuteOkCommand));
			}
		}

		private void ExecuteOkCommand()
		{
			var ng = new NewGrupaCreatedMessage();
			ng.NazwaGrupy = NazwaGrupy;
			MessengerInstance.Send(ng);
		}

		private bool CanExecuteOkCommand()
		{
			return GrupaZajec==null|| GrupaZajec.GrupyZajec.All(x => x.NazwaGrupy != NazwaGrupy);
		}
		private RelayCommand mCancelCommand;

		/// <summary>
		/// Gets the CancelCommand.
		/// </summary>
		public RelayCommand CancelCommand
		{
			get
			{
				return mCancelCommand ?? (mCancelCommand = new RelayCommand(
					ExecuteCancelCommand,
					CanExecuteCancelCommand));
			}
		}

		private void ExecuteCancelCommand()
		{

		}

		private bool CanExecuteCancelCommand()
		{
			return true;
		}
		/// <summary>
		/// The <see cref="NazwaGrupy" /> property's name.
		/// </summary>
		public const string NazwaGrupyPropertyName = "NazwaGrupy";

		private string mNazwaGrupy = string.Empty;

		/// <summary>
		/// Sets and gets the NazwaGrupy property.
		/// Changes to that property's value raise the PropertyChanged event. 
		/// </summary>
		public string NazwaGrupy
		{
			get
			{
				return mNazwaGrupy;
			}
			set
			{
				Set(() => NazwaGrupy, ref mNazwaGrupy, value);
			}
		}
	}
}
