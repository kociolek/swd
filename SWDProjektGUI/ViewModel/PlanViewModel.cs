﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GalaSoft.MvvmLight;
using SWDLibDemo.Model;

namespace SWDProjektGUI.ViewModel
{
	public class PlanViewModel:ViewModelBase
	{
		/// <summary>
		/// The <see cref="Title" /> property's name.
		/// </summary>
		public const string TitlePropertyName = "Title";

		private string mTitle = string.Empty;

		/// <summary>
		/// Sets and gets the Title property.
		/// Changes to that property's value raise the PropertyChanged event. 
		/// </summary>
		public string Title
		{
			get
			{
				return mTitle;
			}
			set
			{
				Set(() => Title, ref mTitle, value);
			}
		}
		/// <summary>
		/// The <see cref="OptymalnyPlan" /> property's name.
		/// </summary>
		public const string OptymalnePlanyPropertyName = "OptymalnyPlan";

		private OptymalnyPlan mOptymalnyPlan = null;

		/// <summary>
		/// Sets and gets the OptymalnyPlan property.
		/// Changes to that property's value raise the PropertyChanged event. 
		/// </summary>
		public OptymalnyPlan OptymalnyPlan
		{
			get
			{
				return mOptymalnyPlan;
			}
			set
			{
				Set(() => OptymalnyPlan, ref mOptymalnyPlan, value);
			}
		}	
	}
}
