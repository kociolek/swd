﻿using System;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Windows;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using SWDLibDemo;
using SWDLibDemo.Model;
using SWDProjektGUI.Messages;
using SWDProjektGUI.Model;

namespace SWDProjektGUI.ViewModel
{
    /// <summary>
    /// This class contains properties that the main View can data bind to.
    /// <para>
    /// See http://www.galasoft.ch/mvvm
    /// </para>
    /// </summary>
    public class MainViewModel : ViewModelBase
    {
        private readonly ISolver mSolver;
        private readonly IDanePlanuRepository mRepo;
	    private DanePlanu mDanePlanu;
	    private GrupaZajec mCurrentGrupa;
	    private TerminZajec mCurrentTermin;

        /// <summary>
        /// The <see cref="WelcomeTitle" /> property's name.
        /// </summary>
        public const string WelcomeTitlePropertyName = "WelcomeTitle";

        private string _welcomeTitle = string.Empty;

		public GrupaZajec CurrentGrupa
		{
			get { return mCurrentGrupa; }
			set { Set(() => CurrentGrupa, ref mCurrentGrupa, value);
				VisGrupa = value == null ? Visibility.Collapsed : Visibility.Visible;
			}
		}

		public TerminZajec CurrentTermin
		{
			get { return mCurrentTermin; }
			set 
			{ 
				if(Set(() => CurrentTermin, ref mCurrentTermin, value) && CurrentTermin!=null)
				{
					CurrentDzien = (int) CurrentTermin.DzienTygodnia;
					CurrentGodzina = (int) (CurrentTermin.GodzinaZajec);
					CurrentOcena = (int) (CurrentTermin.OcenaZajec-1);
				}
				VisTermin = value == null ? Visibility.Collapsed : Visibility.Visible;
			}
		}

        /// <summary>
        /// Gets the WelcomeTitle property.
        /// Changes to that property's value raise the PropertyChanged event. 
        /// </summary>
        public string WelcomeTitle
        {
            get
            {
                return _welcomeTitle;
            }

            set
            {
                if (_welcomeTitle == value)
                {
                    return;
                }

                _welcomeTitle = value;
                RaisePropertyChanged(WelcomeTitlePropertyName);
            }
        }

        /// <summary>
        /// Initializes a new instance of the MainViewModel class.
        /// </summary>
        public MainViewModel(ISolver solver, IDanePlanuRepository repository)
        {
            mSolver = solver;
            mRepo = repository;

            for(int i=0; i<49;i++)
            {
	            var dzien = (DzienTygodnia) (i/7);
	            var godzina = (GodzinaZajec) (i%7);
                mDniGodziny.Add(new DayTimeViewModel(this, dzien,godzina));
				WygladPlanu.Add(new PlanEntryViewModel());
            }
			MessengerInstance.Register<NewGrupaCreatedMessage>(this, ReceiveNewGrupaCreatedMessage);
	        DanePlanu = repository.CreateEmpty();
			if(IsInDesignModeStatic)
			{
				var g = DanePlanu.CreateNewGrupa("Grupa0");
				var t = g.CreateNewTermin("T0", DzienTygodnia.Poniedzialek, GodzinaZajec.Od0915);
				CurrentTermin = t;
			}
        }

	    private void ReceiveNewGrupaCreatedMessage(NewGrupaCreatedMessage newGrupaCreatedMessage)
	    {
		    DanePlanu.CreateNewGrupa(newGrupaCreatedMessage.NazwaGrupy);
	    }

	    private RelayCommand mLoadFileCommand;
	    private RelayCommand mSaveFileCommand;

	    public RelayCommand LoadFileCommand
	    {
		    get
		    {
				return mLoadFileCommand
					?? (mLoadFileCommand = new RelayCommand(ExecuteLoadFileCommand));
		    }
	    }
	    public RelayCommand SaveFileCommand
	    {
		    get
		    {
				return mSaveFileCommand
					?? (mSaveFileCommand = new RelayCommand(ExecuteSaveFileCommand));
		    }
	    }

	    private RelayCommand<object> mSelectedClusterChanged;
	    public RelayCommand<object> SelectedClusterChanged
	    {
			get { return mSelectedClusterChanged??(mSelectedClusterChanged = new RelayCommand<object>(ExecuteSelectedClusterChanged)); }
	    }
		private void ExecuteSelectedClusterChanged(object o)
		{
			CurrentGrupa = o as GrupaZajec;
			CurrentTermin = o as TerminZajec;
		}
		private void ExecuteLoadFileCommand()
		{
			var loadMessage = new Messages.LoadFileMessage("Pliki planu (.xml)|*.xml", (filePath) =>
				                                                        {
					                                                        if (filePath != null)
					                                                        {
						                                                        CurrentFile = filePath;
						                                                        mRepo.GetDane(filePath, (exception, planu) =>
							                                                                                {
								                                                                                if (exception == null)
									                                                                                DanePlanu = planu;
							                                                                                });
					                                                        }
				                                                        });
			MessengerInstance.Send(loadMessage);
		}
		private void ExecuteSaveFileCommand()
		{
			string filePath = null;
			var loadMessage = new Messages.SaveFileMessage("Pliki planu (.xml)|*.xml", (file) =>
				                                                                           {
					                                                                           filePath = file;
				                                                                           });
			MessengerInstance.Send(loadMessage);
			if (!string.IsNullOrWhiteSpace(filePath))
			{
				DanePlanu.Write(filePath);
			}
		}
		public string CurrentFile
		{
			get { return mCurrentFile; }
			set { Set(() => CurrentFile, ref mCurrentFile, value); }
		}

	    ////public override void Cleanup()
        ////{
        ////    // Clean up if needed

        ////    base.Cleanup();
        ////}

        /// <summary>
        /// The <see cref="DniGodziny" /> property's name.
        /// </summary>
        public const string DniGodzinyPropertyName = "DniGodziny";

        private ObservableCollection<DayTimeViewModel> mDniGodziny = new ObservableCollection<DayTimeViewModel>();
	    private string mCurrentFile;

	    /// <summary>
        /// Sets and gets the DniGodziny property.
        /// Changes to that property's value raise the PropertyChanged event. 
        /// </summary>
        public ObservableCollection<DayTimeViewModel> DniGodziny
        {
            get
            {
                return mDniGodziny;
            }
            set
            {
                Set(() => DniGodziny, ref mDniGodziny, value);
            }
        }
		/// <summary>
		/// The <see cref="CurrentPlan" /> property's name.
		/// </summary>
		public const string CurrentPlanPropertyName = "CurrentPlan";

		private PlanViewModel mCurrentPlan = null;

		/// <summary>
		/// Sets and gets the CurrentPlan property.
		/// Changes to that property's value raise the PropertyChanged event. 
		/// </summary>
		public PlanViewModel CurrentPlan
		{
			get
			{
				return mCurrentPlan;
			}
			set
			{
				Set(() => CurrentPlan, ref mCurrentPlan, value);
				
					foreach (var planEntryViewModel in WygladPlanu)
					{
						planEntryViewModel.IsEmpty = true;
						planEntryViewModel.Text = string.Empty;
					}
					if (CurrentPlan != null)
				{
					foreach (var zajecia in CurrentPlan.OptymalnyPlan.Zajecia)
					{
						int index = (int)zajecia.DzienTygodnia*7 + (int)zajecia.GodzinaZajec;
						WygladPlanu[index].IsEmpty = false;
						var ocenaPlusMinus = (int) zajecia.OcenaZajec - 1;

						WygladPlanu[index].Text = string.Format("{0}\r\n{1}({2})\r\n{3}", zajecia.Grupa.NazwaGrupy, zajecia.Opis,
						                                        ocenaPlusMinus == 0
							                                        ? "+/-"
							                                        : ocenaPlusMinus < 0
								                                          ? new string('-', -ocenaPlusMinus)
								                                          : new string('+', ocenaPlusMinus),
						                                        zajecia.KodKursu);
						//zajecia.Grupa.NazwaGrupy + "\r\n" + zajecia.Opis + "\r\n" + zajecia.KodKursu;
					}
				}
			}
		}
	    public DanePlanu DanePlanu
	    {
		    get { return mDanePlanu; }
			set
			{
				var oldValue = DanePlanu;
				if(Set(() => DanePlanu, ref mDanePlanu, value))
				{
					//reset stanow
					if(oldValue!=null)
						oldValue.OptymalnePlany.CollectionChanged -= OptymalnePlanyOnCollectionChanged;
					if (DanePlanu != null)
					{
						DanePlanu.OptymalnePlany.CollectionChanged += OptymalnePlanyOnCollectionChanged;
						OptymalnePlanyOnCollectionChanged(null,null);
					}
				}
			}
	    }

	    private void OptymalnePlanyOnCollectionChanged(object sender, NotifyCollectionChangedEventArgs notifyCollectionChangedEventArgs)
	    {
			OptymalnePlany.Clear();
		    int i = 0;
		    foreach (var optymalnyPlan in DanePlanu.OptymalnePlany)
		    {
			    OptymalnePlany.Add(new PlanViewModel() {OptymalnyPlan = optymalnyPlan, Title = string.Format("Plan Nr {0}", ++i)});
		    }
	    }

	    /// <summary>
		/// The <see cref="VisTermin" /> property's name.
		/// </summary>
		public const string VisTerminPropertyName = "VisTermin";

		private Visibility mVisTermin = Visibility.Collapsed;

		/// <summary>
		/// Sets and gets the VisTermin property.
		/// Changes to that property's value raise the PropertyChanged event. 
		/// </summary>
		public Visibility VisTermin
		{
			get
			{
				return mVisTermin;
			}

			set
			{
				if (mVisTermin == value)
				{
					return;
				}

				RaisePropertyChanging(() => VisTermin);
				mVisTermin = value;
				RaisePropertyChanged(() => VisTermin);
			}
		}
		/// <summary>
		/// The <see cref="VisGrupa" /> property's name.
		/// </summary>
		public const string VisGrupaPropertyName = "VisGrupa";

		private Visibility mVisGrupa = Visibility.Collapsed;

		/// <summary>
		/// Sets and gets the VisGrupa property.
		/// Changes to that property's value raise the PropertyChanged event. 
		/// </summary>
		public Visibility VisGrupa
		{
			get
			{
				return mVisGrupa;
			}

			set
			{
				if (mVisGrupa == value)
				{
					return;
				}

				RaisePropertyChanging(() => VisGrupa);
				mVisGrupa = value;
				RaisePropertyChanged(() => VisGrupa);
			}
		}
		private RelayCommand mAddTerminCommand;

		/// <summary>
		/// Gets the AddTerminCommand.
		/// </summary>
		public RelayCommand AddTerminCommand
		{
			get
			{
				return mAddTerminCommand ?? (mAddTerminCommand = new RelayCommand(
					ExecuteAddTerminCommand,
					CanExecuteAddTerminCommand));
			}
		}

		private void ExecuteAddTerminCommand()
		{
			var termin = CurrentGrupa.CreateNewTermin(string.Format("Nowy ({0})",DateTime.Now), DzienTygodnia.Poniedzialek, GodzinaZajec.Od0730);
			CurrentGrupa = null;
			CurrentTermin = termin;
		}


		private bool CanExecuteAddTerminCommand()
		{
			return CurrentGrupa != null;
		}

		private RelayCommand mAddGrupaCommand;

		/// <summary>
		/// Gets the AddGrupaCommand.
		/// </summary>
		public RelayCommand AddGrupaCommand
		{
			get
			{
				return mAddGrupaCommand ?? (mAddGrupaCommand = new RelayCommand(
					ExecuteAddGrupaCommand,
					CanExecuteAddGrupaCommand));
			}
		}

		private void ExecuteAddGrupaCommand()
		{
			MessengerInstance.Send(new CreateNewGrupaMessage(){GrupaZajec = DanePlanu});
		}

		private bool CanExecuteAddGrupaCommand()
		{
			return true;
		}
		private RelayCommand mRemoveSelectedCommand;

		/// <summary>
		/// Gets the RemoveSelectedCommand.
		/// </summary>
		public RelayCommand RemoveSelectedCommand
		{
			get
			{
				return mRemoveSelectedCommand ?? (mRemoveSelectedCommand = new RelayCommand(
					ExecuteRemoveSelected,
					CanExecuteRemoveSelected));
			}
		}

	    private static readonly string[] mDniTygodnia = new[]
		                                           {
			                                           "Poniedziałek", "Wtorek", "Środa", "Czwartek", "Piątek", "Sobota",
			                                           "Niedziela"
		                                           };
	    public string[] DniTygodnia
	    {
		    get { return mDniTygodnia; }
	    }
		private static readonly string[] mGodzinyZajec = new string[]
			                                        {
				                                        "Od 7:30 do 9:00","Od 9:15 do 11:00","Od 11:15 do 13:00","Od 13:15 do 15:00","Od 15:15 do 17:00","Od 17:05 do 18:45","Od 18:55 do 20:35"
			                                        };
	    public string[] GodzinyZajec
	    {
			get { return mGodzinyZajec; }
	    }
		/// <summary>
		/// The <see cref="CurrentOcena" /> property's name.
		/// </summary>
		public const string CurrentOcenaPropertyName = "CurrentOcena";

		private int mCurrentOcena = 0;

		/// <summary>
		/// Sets and gets the CurrentOcena property.
		/// Changes to that property's value raise the PropertyChanged event. 
		/// </summary>
		public int CurrentOcena
		{
			get
			{
				return mCurrentOcena;
			}
			set
			{
				if(Set(() => CurrentOcena, ref mCurrentOcena, value))
				{
					CurrentTermin.OcenaZajec = (Ocena) (value + 1);
				}
			}
		}
		/// <summary>
		/// The <see cref="CurrentGodzina" /> property's name.
		/// </summary>
		public const string CurrentGodzinaPropertyName = "CurrentGodzina";

		private int mCurrentGodzina = 0;

		/// <summary>
		/// Sets and gets the CurrentGodzina property.
		/// Changes to that property's value raise the PropertyChanged event. 
		/// </summary>
		public int CurrentGodzina
		{
			get
			{
				return mCurrentGodzina;
			}
			set
			{
				if(Set(() => CurrentGodzina, ref mCurrentGodzina, value))
				{
					CurrentTermin.GodzinaZajec = (GodzinaZajec) value;
				}
			}
		}
		/// <summary>
		/// The <see cref="CurrentDzien" /> property's name.
		/// </summary>
		public const string CurrentDzienPropertyName = "CurrentDzien";

	    private int mCurrentDzien = 0;

		/// <summary>
		/// Sets and gets the CurrentDzien property.
		/// Changes to that property's value raise the PropertyChanged event. 
		/// </summary>
		public int CurrentDzien
		{
			get
			{
				return mCurrentDzien;
			}
			set
			{
				if(Set(() => CurrentDzien, ref mCurrentDzien, value))
				{
					CurrentTermin.DzienTygodnia = (DzienTygodnia) value;
				}
			}
		}
		private bool AskDelete(string item)
		{
			MessageBoxResult res = MessageBoxResult.No;

			MessengerInstance.Send(new DialogMessage(string.Format("Czy na pewno chcesz usunąć {0}?", item), (x) =>
				                                                                                                 {
					                                                                                                 res
						                                                                                                 =
						                                                                                                 x;
				                                                                                                 })
				                       {Button = MessageBoxButton.YesNo, Caption = "Uwaga", Icon = MessageBoxImage.Question});
			return res == MessageBoxResult.Yes;
		}

	    private void ExecuteRemoveSelected()
		{
			if(CurrentGrupa != null)
			{
				if (AskDelete(string.Format("termin {0}",CurrentGrupa.NazwaGrupy)))
				{
					DanePlanu.GrupyZajec.Remove(CurrentGrupa);
					CurrentGrupa = null;
				}
			}
			if(CurrentTermin != null)
			{
				if (AskDelete(string.Format(" zajęcia {0}({1})",CurrentTermin.Opis, CurrentTermin.KodKursu)))
				{
					CurrentTermin.Grupa.Terminy.Remove(CurrentTermin);
					CurrentTermin = null;
				}
			}
		}

		private bool CanExecuteRemoveSelected()
		{
			return CurrentGrupa != null || CurrentTermin != null;
		}
		/// <summary>
		/// The <see cref="WygladPlanu" /> property's name.
		/// </summary>
		public const string WygladPlanuPropertyName = "WygladPlanu";

		private ObservableCollection<PlanEntryViewModel> mWygladPlanu = new ObservableCollection<PlanEntryViewModel>();

		/// <summary>
		/// Sets and gets the WygladPlanu property.
		/// Changes to that property's value raise the PropertyChanged event. 
		/// </summary>
		public ObservableCollection<PlanEntryViewModel> WygladPlanu
		{
			get
			{
				return mWygladPlanu;
			}
			set
			{
				Set(() => WygladPlanu, ref mWygladPlanu, value);
			}
		}

		private RelayCommand mMakePlanCommand;

		/// <summary>
		/// Gets the MakePlanCommand.
		/// </summary>
		public RelayCommand MakePlanCommand
		{
			get
			{
				return mMakePlanCommand ?? (mMakePlanCommand = new RelayCommand(
					ExecuteMakePlanCommand,
					CanExecuteMakePlanCommand));
			}
		}

		private void ExecuteMakePlanCommand()
		{
			var solution = mSolver.Solve(DanePlanu);
			if (solution == null || solution.Length == 0)
			{
				MessengerInstance.Send(new GalaSoft.MvvmLight.Messaging.DialogMessage("Nie możliwe jest ułożenie planu przy zadanych parametrach!", (x) =>
																																						{
																																						}) { Caption = "Uwaga", Icon = MessageBoxImage.Error });
			}
			else
			{
				MessengerInstance.Send(new GalaSoft.MvvmLight.Messaging.DialogMessage("Plan ułożony", (x) =>
					                                                                                      {
																											 
					                                                                                      }){Icon = MessageBoxImage.Information,Caption = "Gotowe"});
				DanePlanu.OptymalnePlany.CollectionChanged -= OptymalnePlanyOnCollectionChanged;
				DanePlanu.SetPlan(solution);
				DanePlanu.OptymalnePlany.CollectionChanged += OptymalnePlanyOnCollectionChanged;
				OptymalnePlanyOnCollectionChanged(null,null);
				CurrentTab = 2;
			}
		}

		private bool CanExecuteMakePlanCommand()
		{
			bool jakiesZajecia = false;
			foreach (var grupaZajec in DanePlanu.GrupyZajec)
			{
				jakiesZajecia = grupaZajec.Terminy.Count
				                != 0;
				if(jakiesZajecia) break;
			}
			return DanePlanu!=null && jakiesZajecia;
		}
		/// <summary>
		/// The <see cref="CurrentTab" /> property's name.
		/// </summary>
		public const string CurrentTabPropertyName = "CurrentTab";

		private int mCurrentTab = 0;

		/// <summary>
		/// Sets and gets the CurrentTab property.
		/// Changes to that property's value raise the PropertyChanged event. 
		/// </summary>
		public int CurrentTab
		{
			get
			{
				return mCurrentTab;
			}
			set
			{
				Set(() => CurrentTab, ref mCurrentTab, value);
			}
		}
		/// <summary>
		/// The <see cref="OptymalePlany" /> property's name.
		/// </summary>
		public const string OptymalePlanyPropertyName = "OptymalePlany";

		private ObservableCollection<PlanViewModel> mOptymalnePlany = new ObservableCollection<PlanViewModel>();

		/// <summary>
		/// Sets and gets the OptymalePlany property.
		/// Changes to that property's value raise the PropertyChanged event. 
		/// </summary>
		public ObservableCollection<PlanViewModel> OptymalnePlany
		{
			get
			{
				return mOptymalnePlany;
			}
			set
			{
				Set(() => OptymalnePlany, ref mOptymalnePlany, value);
			}
		}
		private RelayCommand mNewCommand;

		/// <summary>
		/// Gets the NewCommand.
		/// </summary>
		public RelayCommand NewCommand
		{
			get
			{
				return mNewCommand
					?? (mNewCommand = new RelayCommand(ExecuteNewCommand));
			}
		}

		private void ExecuteNewCommand()
		{
			DanePlanu = mRepo.CreateEmpty();
		}
    }
}