namespace SWDLibDemo
{
	public enum DzienTygodnia
	{
		Poniedzialek=0,
		Wtorek=1,
		Sroda=2,
		Czwartek=3,
		Piatek=4,
		Sobota=5,
		Niedziela=6
	}
}