using GalaSoft.MvvmLight;

namespace SWDLibDemo.Model
{
    public class TerminZajec :ObservableObject
    {
		/// <summary>
		/// The <see cref="Grupa" /> property's name.
		/// </summary>
		public const string GrupaPropertyName = "Grupa";

		private GrupaZajec mGrupa = null;

		/// <summary>
		/// Sets and gets the Grupa property.
		/// Changes to that property's value raise the PropertyChanged event. 
		/// </summary>
		public GrupaZajec Grupa
		{
			get
			{
				return mGrupa;
			}

			set
			{
				if (mGrupa == value)
				{
					return;
				}
				if(mGrupa!=null)
				{
					mGrupa.Terminy.Remove(this);
					value.Terminy.Add(this);
				}
				RaisePropertyChanging(() => Grupa);
				mGrupa = value;
				RaisePropertyChanged(() => Grupa);
			}
		}
        /// <summary>
        /// The <see cref="Opis" /> property's name.
        /// </summary>
        public const string OpisPropertyName = "Opis";

        private string mOpis = default(string);

        /// <summary>
        /// Sets and gets the Opis property.
        /// Changes to that property's value raise the PropertyChanged event. 
        /// </summary>
        public string Opis
        {
            get
            {
                return mOpis;
            }
            set
            {
                Set(() => Opis, ref mOpis, value);
            }
        }
        /// <summary>
        /// The <see cref="OcenaZajec" /> property's name.
        /// </summary>
        public const string OcenaZajecPropertyName = "OcenaZajec";

        private Ocena mOcenaZajec = Ocena.Default;

        /// <summary>
        /// Sets and gets the OcenaZajec property.
        /// Changes to that property's value raise the PropertyChanged event. 
        /// </summary>
        public Ocena OcenaZajec
        {
            get
            {
                return mOcenaZajec;
            }
            set
            {
                Set(() => OcenaZajec, ref mOcenaZajec, value);
            }
        }
        /// <summary>
        /// The <see cref="GodzinaZajec" /> property's name.
        /// </summary>
        public const string GodzinaZajecPropertyName = "GodzinaZajec";

        private GodzinaZajec mGodzinaZajec = default(GodzinaZajec);

        /// <summary>
        /// Sets and gets the GodzinaZajec property.
        /// Changes to that property's value raise the PropertyChanged event. 
        /// </summary>
        public GodzinaZajec GodzinaZajec
        {
            get
            {
                return mGodzinaZajec;
            }
            set
            {
                Set(() => GodzinaZajec, ref mGodzinaZajec, value);
            }
        }
        /// <summary>
        /// The <see cref="DzienTygodnia" /> property's name.
        /// </summary>
        public const string DzienTygodniaPropertyName = "DzienTygodnia";

        private DzienTygodnia mDzienTygodnia = default(DzienTygodnia);

        /// <summary>
        /// Sets and gets the DzienTygodnia property.
        /// Changes to that property's value raise the PropertyChanged event. 
        /// </summary>
        public DzienTygodnia DzienTygodnia
        {
            get
            {
                return mDzienTygodnia;
            }
            set
            {
                Set(() => DzienTygodnia, ref mDzienTygodnia, value);
            }
        }
        /// <summary>
        /// The <see cref="KodKursu" /> property's name.
        /// </summary>
        public const string KodKursuPropertyName = "KodKursu";

        private string mKodKursu = default(string);

        /// <summary>
        /// Sets and gets the KodKursu property.
        /// Changes to that property's value raise the PropertyChanged event. 
        /// </summary>
        public string KodKursu
        {
            get
            {
                return mKodKursu;
            }
            set
            {
                Set(() => KodKursu, ref mKodKursu, value);
            }
        }
        internal TerminZajec(string kodKursu, DzienTygodnia dzien, GodzinaZajec godzina, GrupaZajec grupa)
        {
	        mGrupa = grupa;
            mKodKursu = kodKursu;
            mDzienTygodnia = dzien;
            mGodzinaZajec = godzina;
        }
    }
}