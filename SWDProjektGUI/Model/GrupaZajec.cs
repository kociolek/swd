using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Linq;
using GalaSoft.MvvmLight;
using SWDProjektGUI.Model;

namespace SWDLibDemo.Model
{
	public class GrupaZajec : ObservableObject
	{
		private DanePlanu mPlan;
		private ObservableCollection<TerminZajec> mTerminy;
		/// <summary>
		/// The <see cref="NazwaGrupy" /> property's name.
		/// </summary>
		public const string NazwaGrupyPropertyName = "NazwaGrupy";

		private string mNazwaGrupy = default(string);

		/// <summary>
		/// Sets and gets the NazwaGrupy property.
		/// Changes to that property's value raise the PropertyChanged event. 
		/// </summary>
		public string NazwaGrupy
		{
			get
			{
				return mNazwaGrupy;
			}
			set
			{
				Set(() => NazwaGrupy, ref mNazwaGrupy, value);
				CheckState();
			}
		}
		public ObservableCollection<TerminZajec> Terminy { get { return mTerminy; } }
		private List<TerminZajec> mTerminState = new List<TerminZajec>(); 
		internal GrupaZajec(string nazwaGrupy, DanePlanu plan)
		{
			mPlan = plan;
			NazwaGrupy = nazwaGrupy;
			mTerminy = new ObservableCollection<TerminZajec>();
			
			mTerminy.CollectionChanged += TerminyOnCollectionChanged;
		}

		private void TerminyOnCollectionChanged(object sender, NotifyCollectionChangedEventArgs notifyCollectionChangedEventArgs)
		{
			if (notifyCollectionChangedEventArgs.Action == NotifyCollectionChangedAction.Add)
			{
				foreach (var item in notifyCollectionChangedEventArgs.NewItems)
				{
					mTerminState.Add(item as TerminZajec);
				}
			}
			else if (notifyCollectionChangedEventArgs.Action == NotifyCollectionChangedAction.Remove)
				foreach (var item in notifyCollectionChangedEventArgs.OldItems)
					mTerminState.Remove(item as TerminZajec);
			CheckState();
		}

		public TerminZajec CreateNewTermin(string kodKursu, DzienTygodnia dzien, GodzinaZajec godzina)
		{
			if(mPlan.GrupyZajec.Any(grupa=>grupa.Terminy.Any(termin=>termin.KodKursu == kodKursu))) 
				throw new ArgumentException("Kurs o podanym kodzie juz istnieje", "kodKursu");
			if(!Enum.IsDefined(typeof(DzienTygodnia), dzien)) 
				throw new ArgumentException("Dzien tygodnia ma niewlasciwa wartosc", "dzien");
			if (!Enum.IsDefined(typeof(GodzinaZajec), godzina)) 
				throw new ArgumentException("Godzina zaj�� ma niewlasciwa wartosc", "godzina");

			TerminZajec tz = new TerminZajec(kodKursu, dzien, godzina,this);
			mTerminy.Add(tz);
			return tz;
		}
		internal void ConfirmState()
		{
			
		}
		private void CheckState()
		{
			
		}
		private bool mIsChanged;
		public bool IsChanged
		{
			get { return mIsChanged; }
			private set { Set(() => IsChanged, ref mIsChanged, value); }
		}
	}
}