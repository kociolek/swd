using System;

namespace SWDLibDemo
{
	[Flags]
	enum RodajTygodnia
	{
		Parzysty=1<<0,
		Nieparzysty=1<<1,
		Oba = Parzysty | Nieparzysty
	}
}