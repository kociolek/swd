﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Linq;
using System.Xml.Linq;
using GalaSoft.MvvmLight;
using SWDLib;
using SWDLibDemo;
using SWDLibDemo.Model;

namespace SWDProjektGUI.Model
{
	public enum StatusDnia
	{
		Neutralny,
		JakTrzeba,
		Nigdy
	}
	public class DanePlanu : ObservableObject
    {
        private readonly ObservableCollection<GrupaZajec> mGrupyZajec;
        public ObservableCollection<GrupaZajec> GrupyZajec { get { return mGrupyZajec; } }
        public ObservableCollection<OptymalnyPlan> OptymalnePlany { get; private set; }
        public ObservableCollection<TerminDnia> ZajeteTerminy { get;private set; }

		private List<GrupaZajec> mGrupyZajecState = new List<GrupaZajec>();
		private List<OptymalnyPlan> mOptymalnePlanyState = new List<OptymalnyPlan>();
		private List<TerminDnia> mZajeteTerminyState = new List<TerminDnia>();

	    private bool mIsChanged;

	    public bool IsChanged
	    {
		    get
		    {
			    return mIsChanged;
		    }
			private set
			{
				Set(() => IsChanged, ref mIsChanged, value);
			}
	    }
		private void ConfirmState()
		{
			mGrupyZajecState.Clear();
			mGrupyZajecState.AddRange(mGrupyZajec);
			mOptymalnePlanyState.Clear();
			mOptymalnePlanyState.AddRange(OptymalnePlany);
			mZajeteTerminyState.Clear();
			mZajeteTerminyState.AddRange(ZajeteTerminy);
			foreach (var grupaZajec in GrupyZajec)
			{
				grupaZajec.ConfirmState();
			}
			CheckChanges();
		}
        public DanePlanu()
        {
            mGrupyZajec = new ObservableCollection<GrupaZajec>();
            OptymalnePlany= new ObservableCollection<OptymalnyPlan>();
            ZajeteTerminy = new ObservableCollection<TerminDnia>();
			mGrupyZajec.CollectionChanged += GrupyZajecOnCollectionChanged;
			OptymalnePlany.CollectionChanged += OptymalnePlanyOnCollectionChanged;
			ZajeteTerminy.CollectionChanged += ZajeteTerminyOnCollectionChanged;
        }
		private bool Equal<T>(IEnumerable<T> c1, IEnumerable<T> c2 )
		{
			return c1.Count() == c2.Count() && c1.Intersect(c2).Count() == c1.Count();
		}
		private void CheckChanges()
		{
			var equal = Equal(mGrupyZajec,(mGrupyZajecState));
			equal &= Equal(OptymalnePlany, mOptymalnePlanyState);
			equal &= Equal(ZajeteTerminy, mZajeteTerminyState);

			equal &= mGrupyZajec.Any(g => g.IsChanged);

			IsChanged = !equal;
		}
	    private void ZajeteTerminyOnCollectionChanged(object sender, NotifyCollectionChangedEventArgs notifyCollectionChangedEventArgs)
	    {
			if (notifyCollectionChangedEventArgs.Action == NotifyCollectionChangedAction.Add)
			{
				foreach(var item in notifyCollectionChangedEventArgs.NewItems)
				{
					mZajeteTerminyState.Add(item as TerminDnia);
				}
			}
			else if (notifyCollectionChangedEventArgs.Action == NotifyCollectionChangedAction.Remove)
				foreach (var item in notifyCollectionChangedEventArgs.OldItems)
					mZajeteTerminyState.Remove(item as TerminDnia);
			CheckChanges();
	    }

	    private void OptymalnePlanyOnCollectionChanged(object sender, NotifyCollectionChangedEventArgs notifyCollectionChangedEventArgs)
	    {
			if (notifyCollectionChangedEventArgs.Action == NotifyCollectionChangedAction.Add)
			{
				foreach (var item in notifyCollectionChangedEventArgs.NewItems)
				{
					mOptymalnePlanyState.Add(item as OptymalnyPlan);
				}
			}
			else if (notifyCollectionChangedEventArgs.Action == NotifyCollectionChangedAction.Remove)
				foreach (var item in notifyCollectionChangedEventArgs.NewItems)
					mOptymalnePlanyState.Remove(item as OptymalnyPlan);
			CheckChanges();
	    }

	    private void GrupyZajecOnCollectionChanged(object sender, NotifyCollectionChangedEventArgs notifyCollectionChangedEventArgs)
	    {
		    if(notifyCollectionChangedEventArgs.Action == NotifyCollectionChangedAction.Add)
		    {
				foreach (var item in notifyCollectionChangedEventArgs.NewItems)
			    mGrupyZajecState.Add(item as GrupaZajec);
		    }
			else if(notifyCollectionChangedEventArgs.Action == NotifyCollectionChangedAction.Remove)
				foreach (var item in notifyCollectionChangedEventArgs.OldItems)
					mGrupyZajecState.Remove(item as GrupaZajec);
			CheckChanges();
	    }

	    public GrupaZajec CreateNewGrupa(string nazwaGrupy)
        {
            if(mGrupyZajec.Any(grupa=>grupa.NazwaGrupy == nazwaGrupy)) throw new ArgumentException("Grupa o podanej nazwie juz istnieje");
            if(string.IsNullOrWhiteSpace(nazwaGrupy)) throw new ArgumentException("Nazwa nie moze byc pusta");
            GrupaZajec gz = new GrupaZajec(nazwaGrupy, this);
            mGrupyZajec.Add(gz);
            return gz;
        }


        public void Read(string file)
        {
			OptymalnePlany.Clear();
			GrupyZajec.Clear();
			ZajeteTerminy.Clear();

            var xmlDoc = XDocument.Load(file);

            foreach (var grupaNode in xmlDoc.Descendants("GrupaZajec"))
            {
                var grupa = CreateNewGrupa(grupaNode.Attribute("nazwa").Value);
                foreach (var zajeciaNode in grupaNode.Descendants("Zajecia"))
                {
                    var attKod = zajeciaNode.Attribute("kod");
                    var terminNode = zajeciaNode.Element("Termin");
                    var attDzien = terminNode.Attribute("dzien");
                    var attGodzina = terminNode.Attribute("godzina");

                    var attOpis = zajeciaNode.Attribute("opis");
                    var attOcena = zajeciaNode.Attribute("ocena");
                    var zajecia = grupa.CreateNewTermin(attKod.Value, (DzienTygodnia) Enum.Parse(typeof (DzienTygodnia), attDzien.Value),
                                                        (GodzinaZajec) Enum.Parse(typeof (GodzinaZajec), attGodzina.Value));
                    if (attOpis != null)
                        zajecia.Opis = attOpis.Value;
                    if (attOcena != null)
                        zajecia.OcenaZajec = (Ocena) Enum.Parse(typeof (Ocena), attOcena.Value);
                }
            }
            var zajatyTerminNode = xmlDoc.Root.Element("DefinicjaZajetegoTygodnia");
            if (zajatyTerminNode != null)
                foreach (var zajetyTerminNode in zajatyTerminNode.Descendants("Termin"))
                {
                    var attDzien = zajetyTerminNode.Attribute("dzien");
                    var attGodzina = zajetyTerminNode.Attribute("godzina");
	                var attStatus = zajetyTerminNode.Attribute("status");
                    var dzien = (DzienTygodnia) Enum.Parse(typeof (DzienTygodnia), attDzien.Value);
                    var godzina = (GodzinaZajec) Enum.Parse(typeof (GodzinaZajec), attGodzina.Value);
	                var status = (StatusDnia) Enum.Parse(typeof (StatusDnia), attStatus.Value);
                    ZajeteTerminy.Add(new TerminDnia {Dzien = dzien, GodzinaDnia = godzina, Status = status});
                }
            foreach (var planNode in xmlDoc.Descendants("Plan"))
            {
                var plan = new OptymalnyPlan();
                foreach (var zajeciaNode in planNode.Descendants("Zajecia"))
                {
                    plan.Zajecia.Add(GetTermin(zajeciaNode.Attribute("kod").Value));
                }
				OptymalnePlany.Add(plan);
            }
			ConfirmState();
        }

        private TerminZajec GetTermin(string kodKursu)
        {
            return mGrupyZajec.SelectMany(g => g.Terminy).FirstOrDefault(z => z.KodKursu == kodKursu);
        }

        public void Write(string file)
        {

            var sekcjaGrup = new XElement("DefinicjaZajec", from grupa in mGrupyZajec
                                                            select
                                                                new XElement("GrupaZajec",
                                                                             new XAttribute("nazwa", grupa.NazwaGrupy),
                                                                             from zajeciaWgrupie in grupa.Terminy
                                                                             select new XElement("Zajecia",
                                                                                                 new XAttribute("kod", zajeciaWgrupie.KodKursu),
                                                                                                 zajeciaWgrupie.Opis == null
                                                                                                     ? null
                                                                                                     : new XAttribute("opis", zajeciaWgrupie.Opis),
                                                                                                 zajeciaWgrupie.OcenaZajec == Ocena.Default
                                                                                                     ? null
                                                                                                     : new XAttribute("ocena", zajeciaWgrupie.OcenaZajec),
                                                                                                 new XElement("Termin",
                                                                                                              new XAttribute("dzien", zajeciaWgrupie.DzienTygodnia),
                                                                                                              new XAttribute("godzina", zajeciaWgrupie.GodzinaZajec)))
                                                                ));
            var sekcjaTygodnia = ZajeteTerminy.Count == 0
                                     ? null
                                     : new XElement("DefinicjaZajetegoTygodnia", from termin in ZajeteTerminy
                                                                                 select new XElement("Termin",
                                                                                                     new XAttribute("dzien", termin.Dzien),
                                                                                                     new XAttribute("godzina", termin.GodzinaDnia),
																									 new XAttribute("status", termin.Status)));
            var sekcjaPlanow = OptymalnePlany.Count == 0
                                   ? null
                                   : new XElement("DefinicjaOptymalnychPlanow", from plan in OptymalnePlany
                                                                                select new XElement("Plan",
                                                                                                    from zajecia in plan.Zajecia
                                                                                                    select new XElement("Zajecia",
                                                                                                                        new XAttribute("kod", zajecia.KodKursu))));
            var xmlDoc = new XElement("DanePlanu", sekcjaGrup, sekcjaTygodnia, sekcjaPlanow);
            xmlDoc.Save(file);
			ConfirmState();
        }

       
		private void CreatePlanNode(Solver.SolutionNode node, Stack<Decyzja> plan )
		{
			if(node.SubNodes==null)
			{
				var gotowyPlan = plan.ToArray();
				OptymalnyPlan op = new OptymalnyPlan();
				for(int i=0; i<gotowyPlan.Length; i++)
				{
					op.Zajecia.Add(GetTermin(gotowyPlan[i].Opis));
				}
				OptymalnePlany.Add(op);
			}
			else
			for (int i = 0; i < node.SubNodes.Length; i++)
			{
				plan.Push(node.SubNodes[i].Decyzja);
				CreatePlanNode(node.SubNodes[i], plan);
				plan.Pop();
				
			}
		}
		public void SetPlan(Solver.SolutionNode[] plan)
		{
			OptymalnePlany.Clear();
			var plany = new Stack<Decyzja>();
			for (int i = 0; i < plan.Length; i++)
			{
				plany.Push(plan[i].Decyzja);
				CreatePlanNode(plan[i], plany);
				plany.Pop();
			}

		}
		public StatusDnia GetStatusDnia(DzienTygodnia dzien, GodzinaZajec godzina)
		{
			var zt = ZajeteTerminy.FirstOrDefault(x => x.Dzien == dzien && x.GodzinaDnia == godzina);
			if (zt != null)
				return zt.Status;
			return StatusDnia.Neutralny;
			
		}
		public void SetZajetyTermin(DzienTygodnia dzien, GodzinaZajec godzina, StatusDnia status)
		{
			var zt = ZajeteTerminy.FirstOrDefault(x => x.Dzien == dzien && x.GodzinaDnia == godzina);
			if (zt != null)
			{
				if (status == StatusDnia.Neutralny)
					ZajeteTerminy.Remove(zt);
				else
					zt.Status = status;
			}
			else
				ZajeteTerminy.Add(new TerminDnia() {Dzien = dzien, GodzinaDnia = godzina, Status = status});
		}
	}
}
