using SWDLib;

namespace SWDLibDemo
{
    public class Stan : State<Stan,Decyzja>
	{
		public bool[] Plan = new bool[7*7];
		public override Stan AdvanceToNext(Decyzja u)
		{
			Decyzja dec =  u;
			Stan nowy = new Stan();
			Plan.CopyTo(nowy.Plan, 0);
			nowy.Plan[dec.DzienISlot] = true;
			return nowy;
		}

		public override int GetHashCode()
		{
			int hc = Plan.Length;
			for (int i = 0; i < Plan.Length; ++i)
			{
				hc = unchecked(hc*314159 + (Plan[i] ? 1 : 0));
			}
			return hc;
		}

		public override bool Equals(object obj)
		{
			Stan s = (Stan) obj;
			for(int i=0; i<49; i++)
				if(s.Plan[i] != Plan[i]) 
					return false;
			return true;
		}
	}
}