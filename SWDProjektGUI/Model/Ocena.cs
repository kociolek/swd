namespace SWDLibDemo
{
	public enum Ocena
	{
		OcenaMinus3=-3,
		OcenaMinus2=-2,
		OcenaMinus1=-1,
		OcenaZero=0,
		OcenaPlus1=1,
		OcenaPlus2=2,
		OcenaPlus3=3,
		Default = OcenaPlus1
	}
}