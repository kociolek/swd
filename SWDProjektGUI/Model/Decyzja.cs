using SWDLib;

namespace SWDLibDemo
{
	public interface IZajecia
	{
		string Opis { get; }
		DzienTygodnia Dzien { get; }
		GodzinaZajec Slot { get; }
		int Typ { get; }
		int Ocena { get; }
	}

    public class Decyzja :Decission, IZajecia
	{
		public DzienTygodnia Dzien { get { return (DzienTygodnia) (DzienISlot/7); } }
		public GodzinaZajec Slot { get { return (GodzinaZajec) (DzienISlot%7); } }
		public int Typ { get; set; }
		public int Ocena { get; set; }
		public int DzienISlot { get; private set; }

		public string Opis { get; private set; }

		public Decyzja(int dzien, int slot, int typ, int ocena, string opis)
		{
			DzienISlot = dzien*7 + slot;
			Typ = typ;
			Ocena = ocena;
			Opis = opis;
		}

		public override int GetHashCode()
		{
			int hash = 37;
			hash = hash*23 + DzienISlot;
			hash = hash*23 + Typ;
			hash = hash*23 + Ocena.GetHashCode();
			hash = hash*23 + Opis.GetHashCode();
			return hash;
		}

		public override bool Equals(object obj)
		{
			return this == obj;
		}

		private static string[] dni = {"pn", "wt", "�r", "czw", "pt", "so", "nd"};

		private static string[] godziny =
			{
				"7:30 - 9:00", "9:15 - 11:00", "11:15 - 13:00", "13:15 - 15:00", "15:15 - 17:00",
				"17:05 - 18:55", "19:00 - 20:35"
			};
		public override string ToString()
		{
			string dzien = dni[(int)Dzien];
			string godzina = godziny[(int)Slot];

			
			return string.Format("{0} {1} - {2}", dzien, godzina, Typ);
		}
	}
}