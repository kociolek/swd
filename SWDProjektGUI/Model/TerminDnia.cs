using GalaSoft.MvvmLight;
using SWDProjektGUI.Model;

namespace SWDLibDemo.Model
{
    public class TerminDnia : ObservableObject
    {
		/// <summary>
		/// The <see cref="Status" /> property's name.
		/// </summary>
		public const string StatusPropertyName = "Status";

		private StatusDnia mStatusDnia = StatusDnia.Neutralny;

		/// <summary>
		/// Sets and gets the Status property.
		/// Changes to that property's value raise the PropertyChanged event. 
		/// </summary>
		public StatusDnia Status
		{
			get
			{
				return mStatusDnia;
			}
			set
			{
				Set(() => Status, ref mStatusDnia, value);
			}
		}
        /// <summary>
        /// The <see cref="GodzinaDnia" /> property's name.
        /// </summary>
        public const string GodzinaDniaPropertyName = "GodzinaDnia";

        private GodzinaZajec mGodzinaDnia = default(GodzinaZajec);

        /// <summary>
        /// Sets and gets the GodzinaDnia property.
        /// Changes to that property's value raise the PropertyChanged event. 
        /// </summary>
        public GodzinaZajec GodzinaDnia
        {
            get
            {
                return mGodzinaDnia;
            }
            set
            {
                Set(() => GodzinaDnia, ref mGodzinaDnia, value);
            }
        }
        /// <summary>
        /// The <see cref="Dzien" /> property's name.
        /// </summary>
        public const string DzienPropertyName = "Dzien";

        private DzienTygodnia mDzien = default(DzienTygodnia);

        /// <summary>
        /// Sets and gets the Dzien property.
        /// Changes to that property's value raise the PropertyChanged event. 
        /// </summary>
        public DzienTygodnia Dzien
        {
            get
            {
                return mDzien;
            }
            set
            {
                Set(() => Dzien, ref mDzien, value);
            }
        }
    }
}