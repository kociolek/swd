namespace SWDLibDemo
{
	class Zajecia
	{
		private static int id = 0;
		public DzienTygodnia DayOfWeek { get; private set; }
		public GodzinaZajec Godzina { get; private set; }
		public Ocena Ocena {get;private set;}
		public int GrupaZajec { get; private set; }
		public int ID { get; private set; }

		public Zajecia(int grupa,DzienTygodnia dzien, GodzinaZajec godzina, Ocena ocena = Ocena.Default)
		{
			ID = id++;
			DayOfWeek = dzien;
			Godzina = godzina;
			Ocena = ocena;
			GrupaZajec = grupa;
		}
	}
}