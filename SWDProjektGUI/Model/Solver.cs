using System;
using System.Collections.Generic;
using SWDLib;
using SWDLibDemo.Model;
using SWDProjektGUI.Model;
using System.Linq;

namespace SWDLibDemo
{
    public interface IDanePlanuRepository
    {
        void GetDane(string file, Action<Exception, DanePlanu> callback);
        void StoreDane(DanePlanu dp, string file, Action<Exception> callback);
	    DanePlanu CreateEmpty();
    }
    public class DanePlanuRepositoryImpl : IDanePlanuRepository
    {
        public void GetDane(string file, Action<Exception, DanePlanu> callback)
        {
            try
            {
                DanePlanu dp = new DanePlanu();
                dp.Read(file);
                callback(null, dp);
            }
            catch (Exception e)
            {
                callback(e, null);
           
            }
        }

        public void StoreDane(DanePlanu dp, string file, Action<Exception> callback)
        {
            try
            {
                dp.Write(file);
                callback(null);
            }
            catch (Exception e)
            {

                callback(e);
            }
        }

	    public DanePlanu CreateEmpty()
	    {
		    return  new DanePlanu();
	    }
    }
    public interface ISolver
    {
        Solver.SolutionNode[] Solve(DanePlanu dp);
    }

    public class SolverWrapper : ISolver
    {
        private Solver mSolver;

        public SolverWrapper()
        {
            mSolver = new Solver();
        }

        public DpSolver<Stan, Decyzja>.SolutionNode[] Solve(DanePlanu dp)
        {
            mSolver.Clear();
            mSolver.N = dp.GrupyZajec.Count;
            for (int i = 0; i < mSolver.N; i++)
            {
                for (int zaj = 0; zaj < dp.GrupyZajec[i].Terminy.Count; zaj++)
                {
                    var termin = dp.GrupyZajec[i].Terminy[zaj];
                    mSolver.DodajZajecia(i, termin.DzienTygodnia, termin.GodzinaZajec, termin.KodKursu, termin.OcenaZajec);
                }
            }
            foreach (var terminDnia in dp.ZajeteTerminy)
            {
                mSolver.SetOcenaDnia(terminDnia.Dzien, terminDnia.GodzinaDnia, (int)terminDnia.Status);
            }
            return mSolver.Solve();
        }
    }

    public class Solver : SWDLib.DpSolver<Stan,Decyzja>
	{

		//private readonly List<Decyzja> mDecyzje = new List<Decyzja>();
	    private List<Decyzja>[] mDecyzje = new List<Decyzja>[0];// = new List<Decyzja>[]; 
		private readonly float[] mOcenaDni;
		public Solver()
		{
			mOcenaDni = new float[7*7];
			for (var i = 0; i < mOcenaDni.Length; i++)
				mOcenaDni[i] = 1;
		}
		public int GetOcenaDnia(DzienTygodnia dzien, GodzinaZajec godzina)
		{
			var ocena = mOcenaDni[(int) dzien*7 + (int) godzina];
			if (ocena == 0) return 1;
			if (ocena == float.NegativeInfinity) return 2;
			return 0;
		}
		public void SetOcenaDnia(DzienTygodnia dzien, GodzinaZajec godzina, int mozliwe)
		{
			float ocena= 1;
			if (mozliwe == 1) 
				ocena = 0;
			else if (mozliwe == 2) ocena = float.NegativeInfinity;
			mOcenaDni[(int) dzien*7 + (int) godzina] = ocena;
		}
		public void DodajZajecia(int grupaZajec, DzienTygodnia dzien, GodzinaZajec godzina,string opis, Ocena ocena=Ocena.Default)
		{
			var dec = new Decyzja((int) dzien, (int) godzina, grupaZajec, (int) ocena, opis);
			mDecyzje[grupaZajec].Add(dec);
		}
		public IList<IZajecia> Zajecia
		{
			get
			{
				return mDecyzje.SelectMany(group => group).ToList().ConvertAll(z=>(IZajecia)z);
				//return mDecyzje.ConvertAll(d=>(IZajecia)d);
			}
		}
		protected override List<Decyzja> GetDicissions(int n, Stan xn)
		{
			var retVal = new List<Decyzja>();
			Stan s = xn;
			foreach (var decission in mDecyzje[n])
			{
				if (!s.Plan[decission.DzienISlot]) retVal.Add(decission);
			}
			/*foreach (var decission in mDecyzje)
			{
				if (decission.Typ == n && 
				    s.Plan[decission.DzienISlot] == false)
					retVal.Add(decission);
			}*/

			return retVal;
		}
		protected override void NChanged(int oldN)
		{
			Array.Resize(ref mDecyzje, N);
			for(int i=0; i<N;i++)
				if(mDecyzje[i] == null)
				mDecyzje[i] = new List<Decyzja>();
				else
					mDecyzje[i].Clear();
		}
		protected override void ClearImpl()
		{
			for (int i = 0; i < 49; i++)
				mOcenaDni[i] = 1;
			for (int i = 0; i < N;i++ )
				mDecyzje[i].Clear();
		}
		private const int OcenaDzienMozliwy = 1;
		private const int OcenaDzienNiemozliwy = int.MinValue;
	    private const int OcenaDzienJakTrzeba = 0;
		public override float Rank(Stan xn, Decyzja un)
		{
			var ocena = mOcenaDni[un.DzienISlot];
			if (ocena == 0) 
				return Math.Min(0, un.Ocena);
			if(ocena < 0) 
				return ocena;
			return un.Ocena*(mOcenaDni[un.DzienISlot]);// ? OcenaDzienMozliwy : OcenaDzienNiemozliwy);
		}

		protected override Stan GenerateEmptyState()
		{
			return new Stan();
		}

	}
}