using System.Collections.Generic;
using GalaSoft.MvvmLight;

namespace SWDLibDemo.Model
{
    public class OptymalnyPlan : ObservableObject
    {
        /// <summary>
        /// The <see cref="Zajecia" /> property's name.
        /// </summary>
        public const string ZajeciaPropertyName = "Zajecia";

        private IList<TerminZajec> mZajecia = new List<TerminZajec>();

        /// <summary>
        /// Sets and gets the Zajecia property.
        /// Changes to that property's value raise the PropertyChanged event. 
        /// </summary>
        public IList<TerminZajec> Zajecia
        {
            get
            {
                return mZajecia;
            }
            set
            {
                Set(() => Zajecia, ref mZajecia, value);
            }
        }
    }
}