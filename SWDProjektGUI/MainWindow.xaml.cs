﻿using System;
using System.Windows;
using GalaSoft.MvvmLight.Messaging;
using SWDProjektGUI.Messages;
using SWDProjektGUI.ViewModel;

namespace SWDProjektGUI
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        /// <summary>
        /// Initializes a new instance of the MainWindow class.
        /// </summary>
        public MainWindow()
        {
            InitializeComponent();
            Closing += (s, e) => ViewModelLocator.Cleanup();
			GalaSoft.MvvmLight.Messaging.Messenger.Default.Register<LoadFileMessage>(this,ReceiveLoadFileMessage );
			GalaSoft.MvvmLight.Messaging.Messenger.Default.Register<SaveFileMessage>(this, ReceiveSaveFileMessage);
			GalaSoft.MvvmLight.Messaging.Messenger.Default.Register<CreateNewGrupaMessage>(this, ReceiveCreateNewGrupaMessage);
	        GalaSoft.MvvmLight.Messaging.Messenger.Default.Register<DialogMessage>(this, ReceiveDialogMessage);
        }

	    private void ReceiveDialogMessage(DialogMessage dialogMessage)
	    {
			var result = MessageBox.Show(dialogMessage.Content, dialogMessage.Caption, dialogMessage.Button, dialogMessage.Icon, 
		                    dialogMessage.DefaultResult);
			dialogMessage.ProcessCallback(result);
	    }

	    private void ReceiveCreateNewGrupaMessage(CreateNewGrupaMessage createNewGrupaMessage)
	    {
		    var view = new NewGrupa();
		    view.DataContext = new NewGrupaViewModel() {GrupaZajec = createNewGrupaMessage.GrupaZajec};
		    view.ShowDialog();
	    }

	    private void ReceiveSaveFileMessage(SaveFileMessage sfm)
		{
			var dlg = new Microsoft.Win32.SaveFileDialog();

			//dlg.DefaultExt = lfm.Notification;
			dlg.Filter = sfm.Notification;
			// Display OpenFileDialog by calling ShowDialog method
			bool? result = dlg.ShowDialog();
			sfm.Execute(dlg.FileName);
		}
		private void ReceiveLoadFileMessage(LoadFileMessage lfm)
		{

			var dlg = new Microsoft.Win32.OpenFileDialog();

			//dlg.DefaultExt = lfm.Notification;
			dlg.Filter = lfm.Notification;
			dlg.Multiselect = false;
			// Display OpenFileDialog by calling ShowDialog method
			bool? result = dlg.ShowDialog();
			lfm.Execute(dlg.FileName);
			/*
// Get the selected file name and display in a TextBox
if (result == true)
{
    // Open document
    string filename = dlg.FileName;
    FileNameTextBox.Text = filename;
 }*/
		}
    }
}