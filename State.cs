﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SWDLib
{
    public abstract class State<TStan, TDecyzja>
        where TDecyzja : Decission
        where TStan : State<TStan, TDecyzja>
    {
        public abstract TStan AdvanceToNext(TDecyzja u);

        public override abstract int GetHashCode();
        public override abstract bool Equals(object obj);
    }
}
